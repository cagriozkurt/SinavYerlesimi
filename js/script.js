let history = [];
let currentActionIndex = -1;
const defaultSeatColor = '#cccccc';
let currentLectureHall = 'farma_1';
let hallStates = {};
let examName = '';
let examDate = '';


document.addEventListener('DOMContentLoaded', function () {
    loadSVG('farma_1.svg');
    document.getElementById('exam-name').addEventListener('input', function () {
        examName = this.value;
        updateAllSVGText();
    });
    document.getElementById('exam-date').addEventListener('change', function () {
        examDate = this.value;
        updateAllSVGText();
    });
    // document.getElementById('auto-fill-seats').addEventListener('click', function () {
    //     const examTakers = parseInt(document.getElementById('exam-takers').value, 10);
    //     if (isNaN(examTakers) || examTakers <= 0) {
    //         alert('Please enter a valid number of exam takers.');
    //         return;
    //     }
    //     autoFillSeats(examTakers);
    // });
});

function updateAllSVGText() {
    const svgElements = document.querySelectorAll('#svg-container svg');
    svgElements.forEach(svgElement => {
        addTextToSVG(svgElement, examName, examDate);
    });
}

function addTextToSVG(svgElement, name, date) {
    if (!svgElement) return;

    const svgNS = svgElement.namespaceURI;
    const width = svgElement.viewBox.baseVal.width;

    createTextElement(svgElement, svgNS, 'exam-name-text', name, 20, 30);
    createTextElement(svgElement, svgNS, 'exam-date-text', date, width - 200, 30);
}

function createTextElement(svg, svgNS, id, text, x, y) {
    let textElement = svg.querySelector(`#${id}`);
    if (!textElement) {
        textElement = document.createElementNS(svgNS, 'text');
        textElement.setAttribute('id', id);
        svg.appendChild(textElement);
    }

    textElement.setAttribute('x', x);
    textElement.setAttribute('y', y);
    textElement.textContent = text;
    textElement.setAttribute('font-family', 'Arial, sans-serif');
    textElement.setAttribute('font-size', '20');
    textElement.setAttribute('fill', 'black');
}


document.getElementById('hall-selector').addEventListener('change', function () {
    loadSVG(this.value);
});

function loadSVG(svgFile) {
    const hallName = svgFile.replace('.svg', '');

    if (currentLectureHall && document.querySelector('#svg-container svg')) {
        saveCurrentState(currentLectureHall);
    }

    currentLectureHall = hallName;
    const svgPath = `assets/svg/${svgFile}`;

    return new Promise((resolve, reject) => {
        if (hallStates[currentLectureHall]) {
            document.getElementById('svg-container').innerHTML = hallStates[currentLectureHall].svgContent;
            initializeSeats();
            updateAllSVGText();
            restoreState(currentLectureHall);
            resolve();
        } else {
            fetch(svgPath)
                .then(response => response.text())
                .then(svg => {
                    document.getElementById('svg-container').innerHTML = svg;
                    initializeSeats();
                    updateAllSVGText();
                    resolve();
                })
                .catch(error => reject(error));
        }
    });
}


function saveCurrentState(hallName) {
    const seats = document.querySelectorAll('#svg-container rect');
    hallStates[hallName] = {
        svgContent: document.getElementById('svg-container').innerHTML,
        seatColors: Array.from(seats).map(seat => seat.style.fill)
    };
}

function restoreState(hallName) {
    const state = hallStates[hallName];
    if (state && state.seatColors) {
        const seats = document.querySelectorAll('#svg-container rect');
        seats.forEach((seat, index) => {
            seat.style.fill = state.seatColors[index];
        });
        updateSeatCounts();
    }
}

function initializeSeats() {
    const svgElement = document.getElementById('svg-container').querySelector('svg');
    const seats = svgElement.querySelectorAll('rect');
    seats.forEach(seat => {
        seat.addEventListener('click', () => fillSeat(seat));
    });

    const svgHeight = svgElement.viewBox.baseVal.height;
    const svgWidth = svgElement.viewBox.baseVal.width;
    const baseY = svgHeight - 250;
    const fontSize = 30;
    const counters = ['A', 'B', 'None'];

    counters.forEach((counter, index) => {
        let textElement = svgElement.querySelector(`#count-${counter.toLowerCase()}`);

        if (!textElement) {
            textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
            textElement.setAttribute("id", `count-${counter.toLowerCase()}`);
            svgElement.appendChild(textElement);
        }

        textElement.setAttribute("x", svgWidth / 2);
        textElement.setAttribute("y", baseY + (fontSize * index));
        textElement.setAttribute("text-anchor", "middle");
        textElement.setAttribute("font-size", fontSize);
        textElement.textContent = `${counter}: 0`;
    });

    updateSeatCounts();
}

// function autoFillSeats(examTakers) {
//     const seats = document.querySelectorAll('#svg-container rect');
//     const rows = {};

//     seats.forEach(seat => {
//         const match = seat.id.match(/rl_(\d+)_(\d+)/);
//         if (match) {
//             const row = parseInt(match[1], 10);
//             if (!rows[row]) {
//                 rows[row] = [];
//             }
//             rows[row].push(seat);
//         }
//     });

//     let filledSeats = 0;
//     const rowKeys = Object.keys(rows).sort((a, b) => a - b);

//     for (const row of rowKeys) {
//         if (filledSeats >= examTakers) break;

//         for (let i = 0; i < rows[row].length; i += 2) {
//             if (filledSeats >= examTakers) break;

//             const seat = rows[row][i];
//             fillSeat(seat, 'A');
//             filledSeats++;
//         }
//     }

//     if (filledSeats < examTakers) {
//         console.error('Not enough seats available to place all exam takers while maintaining optimal distance.');
//     }
// }


function fillSeat(seat) {
    const version = document.getElementById('version-selector').value;
    const colorMap = {
        'A': '#ff0000', // red
        'None': '#67dd9f', // green
        'B': '#0000ff', // blue
        'Empty': '#cccccc' // grey
    };
    recordAction(seat.id, seat.style.fill, colorMap[version]);
    seat.style.fill = colorMap[version] || '#cccccc';
    updateSeatCounts();
}

// function fillSeat(seat, version = 'A') {
//     const colorMap = {
//         'A': '#ff0000',
//         'None': '#67dd9f',
//         'B': '#0000ff',
//         'Empty': '#cccccc'
//     };
//     seat.style.fill = colorMap[version] || '#cccccc';
//     updateSeatCounts();
// }

function recordAction(seatId, previousColor, newColor) {
    if (currentActionIndex < history.length - 1) {
        history = history.slice(0, currentActionIndex + 1);
    }

    history.push({ seatId, previousColor, newColor });
    currentActionIndex++;
}

document.getElementById('file-upload-btn').addEventListener('click', function () {
    document.getElementById('upload-design').click();
});

document.getElementById('upload-design').addEventListener('change', function () {
    const fileNameField = document.getElementById('file-name');
    const files = this.files;
    fileNameField.textContent = files.length > 0 ? files[0].name : 'Dosya seçilmedi';
});


function undo() {
    if (currentActionIndex < 0) return;

    const action = history[currentActionIndex];
    const seat = document.getElementById(action.seatId);
    seat.style.fill = action.previousColor;
    currentActionIndex--;

    updateSeatCounts();
}


function redo() {
    if (currentActionIndex >= history.length - 1) return;

    const action = history[currentActionIndex + 1];
    const seat = document.getElementById(action.seatId);
    seat.style.fill = action.newColor;
    currentActionIndex++;

    updateSeatCounts();
}


document.getElementById('clear-seats').addEventListener('click', clearSeats);
function clearSeats() {
    const seats = document.querySelectorAll('#svg-container rect');
    seats.forEach(seat => {
        recordAction(seat.id, seat.style.fill, defaultSeatColor);
        seat.style.fill = defaultSeatColor;
    });
    currentActionIndex = history.length - 1;
    updateSeatCounts();
}


function downloadPNG() {
    const svgElement = document.querySelector('#svg-container svg');
    const svgData = new XMLSerializer().serializeToString(svgElement);

    const canvas = document.createElement('canvas');
    canvas.width = svgElement.viewBox.baseVal.width || svgElement.clientWidth;
    canvas.height = svgElement.viewBox.baseVal.height || svgElement.clientHeight;
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const img = new Image();
    img.onload = () => {
        ctx.drawImage(img, 0, 0);

        canvas.toBlob(blob => {
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download = `${currentLectureHall}.png`;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        });
    };

    img.src = 'data:image/svg+xml;base64,' + btoa(decodeURIComponent(encodeURIComponent(svgData)));
}

function updateSeatCounts() {
    const seats = document.querySelectorAll('#svg-container rect');
    let countA = 0;
    let countNone = 0;
    let countB = 0;

    const colorMap = {
        'A': 'rgb(255, 0, 0)', // Red in RGB
        'None': 'rgb(103, 221, 159)', // Green in RGB
        'B': 'rgb(0, 0, 255)' // Blue in RGB
    };

    seats.forEach(seat => {
        const seatStyle = window.getComputedStyle(seat);
        const seatColor = seatStyle.fill || seatStyle.getPropertyValue("fill");

        if (seatColor === colorMap['A']) {
            countA++;
        } else if (seatColor === colorMap['None']) {
            countNone++;
        } else if (seatColor === colorMap['B']) {
            countB++;
        }
    });
    document.getElementById('count-a').textContent = `A: ${countA}`;
    document.getElementById('count-none').textContent = `Yok: ${countNone}`;
    document.getElementById('count-b').textContent = `B: ${countB}`;
}

function saveDesign() {
    const seats = document.querySelectorAll('#svg-container rect');
    const seatColors = Array.from(seats).map(seat => seat.style.fill);
    const design = {
        hallName: currentLectureHall,
        seatColors: seatColors
    };

    const designBlob = new Blob([JSON.stringify(design)], { type: 'application/json' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(designBlob);
    link.download = `${currentLectureHall}-seating-design.json`;
    link.click();
    link.remove();
}

function uploadDesign(event) {
    const fileInput = event.target;
    const file = fileInput.files[0];
    const uploadWarning = document.getElementById('upload-warning');

    if (file) {
        const reader = new FileReader();
        reader.onload = function (fileEvent) {
            try {
                const design = JSON.parse(fileEvent.target.result);
                if (design.hallName && design.seatColors) {
                    if (design.hallName !== currentLectureHall) {
                        uploadWarning.textContent = `Uyarı. Yüklenen yerleşim düzeni: ${design.hallName}, seçili sınav salonu: ${currentLectureHall}`
                        uploadWarning.classList.add('flicker');

                        setTimeout(() => {
                            uploadWarning.textContent = '';
                            uploadWarning.classList.remove('flicker');
                        }, 3000);
                        fileInput.value = '';
                        return;
                    }
                    const seats = document.querySelectorAll('#svg-container rect');
                    if (seats.length === design.seatColors.length) {
                        seats.forEach((seat, index) => {
                            seat.style.fill = design.seatColors[index];
                        });
                        updateSeatCounts();
                    }
                }
            } catch (error) {
                console.error('Error loading design:', error);
                uploadWarning.textContent = 'Error loading design. Please check the file and try again.';
                uploadWarning.classList.add('flicker');
                setTimeout(() => {
                    uploadWarning.textContent = '';
                    uploadWarning.classList.remove('flicker');
                }, 3000);
                fileInput.value = '';
            }
        };
        reader.readAsText(file);
    }
}


document.getElementById('upload-design').addEventListener('change', uploadDesign);
document.getElementById('load-ab').addEventListener('click', () => loadExampleDesign('a_b'));
document.getElementById('load-yok').addEventListener('click', () => loadExampleDesign('yok'));

function loadExampleDesign(type) {
    const hall = document.getElementById('hall-selector').value.replace('.svg', '');
    const exampleFilePath = `assets/examples/${type}/${hall}.json`;
    fetch(exampleFilePath)
        .then(response => response.json())
        .then(design => {
            if (design && design.seatColors) {
                const seats = document.querySelectorAll('#svg-container rect');
                if (seats.length === design.seatColors.length) {
                    seats.forEach((seat, index) => {
                        seat.style.fill = design.seatColors[index];
                    });
                    updateSeatCounts();
                }
            }
        })
        .catch(error => console.error('Error loading example design:', error));
}
